From 2d064a45017636b9492c5c252516d789cabaf6a9 Mon Sep 17 00:00:00 2001
From: "A. Maitland Bottoms" <bottoms@debian.org>
Date: Sun, 5 Dec 2021 21:11:33 -0500
Subject: [PATCH] cmake check march

Some build platforms do not have the -march=native compiler option.
This commit adds CMake checks to avoid an unrecognized option error.
---
 host/lib/transport/uhd-dpdk/CMakeLists.txt |  6 +++++-
 host/tests/CMakeLists.txt                  | 12 +++++++++++-
 2 files changed, 16 insertions(+), 2 deletions(-)

--- a/host/lib/transport/uhd-dpdk/CMakeLists.txt
+++ b/host/lib/transport/uhd-dpdk/CMakeLists.txt
@@ -10,8 +10,12 @@
 if(ENABLE_DPDK)
     if(NOT DEFINED UHD_DPDK_CFLAGS)
         message(STATUS "")
-        set(UHD_DPDK_CFLAGS "-march=native"
+        include(CheckCXXCompilerFlag)
+        check_cxx_compiler_flag("-march=native" HAVE_CXX_MARCH_NATIVE)
+	if (HAVE_CXX_MARCH_NATIVE)
+          set(UHD_DPDK_CFLAGS "-march=native"
             CACHE STRING "CFLAGS to use when building uhd-dpdk sources")
+	endif(HAVE_CXX_MARCH_NATIVE)
         message(STATUS "DPDK: Using default UHD_DPDK_CFLAGS=" ${UHD_DPDK_CFLAGS})
     endif(NOT DEFINED UHD_DPDK_CFLAGS)
 
--- a/host/tests/CMakeLists.txt
+++ b/host/tests/CMakeLists.txt
@@ -191,11 +191,18 @@
         EXTRA_LIBS ${DPDK_LIBRARIES}
         NOAUTORUN # Don't register for auto-run, it requires special config
     )
+    include(CheckCXXCompilerFlag)
+    check_cxx_compiler_flag("-march=native" HAVE_CXX_MARCH_NATIVE)
+    if (HAVE_CXX_MARCH_NATIVE)
+      set(UHD_DPDK_COMPILE_FLAGS "-march=native -D_GNU_SOURCE")
+    else()
+      set(UHD_DPDK_COMPILE_FLAGS "-D_GNU_SOURCE")
+    endif(HAVE_CXX_MARCH_NATIVE)
     set_source_files_properties(
         ${UHD_SOURCE_DIR}/lib/transport/uhd-dpdk/dpdk_common.cpp
         ${UHD_SOURCE_DIR}/lib/transport/uhd-dpdk/dpdk_io_service.cpp
         ${UHD_SOURCE_DIR}/lib/transport/udp_dpdk_link.cpp
-        PROPERTIES COMPILE_FLAGS "-march=native -D_GNU_SOURCE"
+        PROPERTIES COMPILE_FLAGS ${UHD_DPDK_COMPILE_FLAGS}
     )
 ENDIF(ENABLE_DPDK)
 
